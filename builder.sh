#!/bin/bash

set -e

cd ../

# Setting environment
ARCHITECTURES_DEFAULT=" \
    ath79-generic \
    ath79-nand \
    ath79-mikrotik \
    bcm27xx-bcm2708 \
    bcm27xx-bcm2709 \
    ipq40xx-generic \
    ipq40xx-mikrotik \
    ipq806x-generic \
    lantiq-xrx200 \
    lantiq-xway \
    mediatek-mt7622 \
    mpc85xx-p1010 \
    mpc85xx-p1020 \
    ramips-mt7620 \
    ramips-mt7621 \
    ramips-mt76x8 \
    rockchip-armv8 \
    sunxi-cortexa7 \
    x86-generic \
    x86-geode \
    x86-legacy \
    x86-64 \
"

BRANCH_DEFAULT=experimental

if [ -z "${BRANCH}" ]; then
	BRANCH=$BRANCH_DEFAULT
fi
if [ -z "${ARCHITECTURES}" ]; then
	ARCHITECTURES=$ARCHITECTURES_DEFAULT
fi

# prepare needed strings
GLUONVERSION=$(git describe --tags)
SITECOMMIT=$(cd site; git log -1 --format="%H")
ARCHSTRING=multiarch
archarray=( $ARCHITECTURES )
if [ ${#archarray[@]} -eq "1" ]; then
  ARCHSTRING=${ARCHITECTURES[0]}
fi
RELEASENAME=${GLUONVERSION}-$(date '+%Y%m%d%H%M')-${BRANCH}
RELEASETAG=${GLUONVERSION}_${BRANCH}_${ARCHSTRING}_$(date '+%Y%m%d-%H%M')_${SITECOMMIT}

if [ -n "${NODEBUG}" ]; then
	DEBUG=""
else
	DEBUG="V=s"
fi

# Days until the autoupdater makes sure that the new vesion gets installed
# (maybe define based on the branch?)
PRIO=0

# how parallel do you want to build?
if [ -z "${J}" ]; then
	J=10
fi

# remove old images to make sure we only upload the new shiny stuff
rm -rf output/images
mkdir -p output
mkdir -p output/cmd

function print_info () {
    echo "#######################################"
    echo "#######################################"
    echo Welcome! This is the Freifunk Braunschweig Gluon Bulder script. Nice to serve you
    echo - Building for the following GLUON_AUTOUPDATER_BRANCH:  \"${BRANCH}\"
    echo - The release will be tagged: ${RELEASETAG}
    echo - The release will be namend: ${RELEASENAME}
    echo - Bulding with j=${J}
    echo - Debug-Config: \"${DEBUG}\"
    echo - Building for the following architectures: ${ARCHITECTURES}
    echo "#######################################"
    echo "#######################################"
}
print_info

# update toolchain to current release
make update


for ARCHITECTURE in ${ARCHITECTURES}
do
    echo "#######################################"
    echo "#######################################"
    echo Building $ARCHITECTURE

    unset BROKEN
    if [ "$ARCHITECTURE" = "ar71xx-mikrotik" ]; then
        if [[ "$BRANCH" == *"beta" ]]; then
            echo "Allowing to bild ar71xx-mikrotik since we build for beta"
	else
	    echo "Skipping build for ar71xx-mikrotik since we do not build for beta"
	    continue
	fi
	BROKEN="BROKEN=1"
    fi
    if [ "$ARCHITECTURE" = "lantiq-xway" ]; then
        if [[ "$BRANCH" == *"beta" ]]; then
            echo "Allowing to bild lantiq-xway with BROKEN=1 since we build for beta"
	else
	    echo "Building lantiq-xway without broken"
	    continue
	fi
	BROKEN="BROKEN=1"
    fi
    echo Building with BROKEN=$BROKEN
    echo "#######################################"
    echo "#######################################"

    # Printing out command
    # ####################
    echo make clean $BROKEN GLUON_TARGET=$ARCHITECTURE $DEBUG -j${J} > output/cmd/${ARCHITECTURE}_make
    echo make all $BROKEN GLUON_TARGET=$ARCHITECTURE $DEBUG -j${J} GLUON_RELEASE=${RELEASENAME} GLUON_AUTOUPDATER_BRANCH=$BRANCH GLUON_AUTOUPDATER_ENABLED=true GLUON_PRIORITY=$PRIO >> output/cmd/${ARCHITECTURE}_make

    # Preparing build
    # ###############
    make clean $BROKEN GLUON_TARGET=$ARCHITECTURE $DEBUG -j${J}
    echo \# make clean completed >> output/cmd/${ARCHITECTURE}_make

    # Doing the Build
    # ###############
    make all \
	    $BROKEN \
	    GLUON_TARGET=$ARCHITECTURE \
	    $DEBUG \
	    -j${J} \
	    GLUON_RELEASE=${RELEASENAME} \
	    GLUON_AUTOUPDATER_BRANCH=$BRANCH \
	    GLUON_AUTOUPDATER_ENABLED=1 \
	    GLUON_PRIORITY=$PRIO \
	    GLUON_DEPRECATED=full

    echo \# make all completed >> output/cmd/${ARCHITECTURE}_make

    echo Exiting with Code $?
done

# build manifest
# ##############
echo making manifest
make manifest GLUON_AUTOUPDATER_BRANCH=$BRANCH GLUON_PRIORITY=${PRIO} GLUON_RELEASE=${RELEASENAME}


# write the current site-commit as into-text
# ###############
echo writing git info into the output directory
(cd site/; git show > ../output/images/commit.txt )

# rename image dir to represent build
echo Resolving symlinks...
cp -Lr output/images "output/${RELEASETAG}"
rm -rf output/images
echo ... done

# shorten resulting image names for some TP-Link routers
echo Shortening filenames
(cd output/${RELEASETAG}/factory; for f in *; do mv "$f" "${f#gluon-ffbs-}"; done )

# rename files to match the ffbs-naming conventions
# TODO: give explanation or further resources
echo Renaming files for consistent naming scheme
(cd output/${RELEASETAG}/factory; for f in *raspberry-pi.img.gz; do mv "$f" "${f/raspberry-pi.img.gz/raspberry-pi-1.img.gz}"; done ) || true
(cd output/${RELEASETAG}/factory; for f in *raspberry-pi-2.img.gz; do cp "$f" "${f/raspberry-pi-2.img.gz/raspberry-pi-3.img.gz}"; done ) || true

(cd output/${RELEASETAG}/factory; for f in *x86-64.vmdk; do mv "$f" "${f/.vmdk/-vmware.vmdk}"; done ) || true
(cd output/${RELEASETAG}/factory; for f in *x86-64.vdi; do mv "$f" "${f/.vdi/-virtualbox.vdi}"; done ) || true
(cd output/${RELEASETAG}/factory; for f in *x86-64.img.gz; do mv "$f" "${f/.img.gz/-generic.img.gz}"; done ) || true

(cd output/${RELEASETAG}/factory; for f in *x86-generic.vmdk; do mv "$f" "${f/x86-generic/x86-vmware}"; done ) || true
(cd output/${RELEASETAG}/factory; for f in *x86-generic.vdi; do mv "$f" "${f/x86-generic/x86-virtualbox}"; done ) || true

# create md5sums
(cd output/${RELEASETAG}/factory; md5sum * > MD5SUMS) || true
(cd output/${RELEASETAG}/sysupgrade; md5sum * > MD5SUMS) || true
(cd output/${RELEASETAG}/other; md5sum * > MD5SUMS) || true

# add debug kernels to output
mv output/debug output/${RELEASETAG}/

# add build commands to output
mv output/cmd output/${RELEASETAG}/

print_info
